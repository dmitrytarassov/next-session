import React, { useCallback, useState, useReducer, useEffect } from 'react';
import axios from 'axios';

const initialState = {session: {}};
function reducer(state, action) {
  if (action.type === 'SET_SESSION') {
    return {
      ...state,
      session: action.payload
    };
  }else {
    throw new Error();
  }
}

const withSessionDecorator = Component => props => {
  const { session = {} } = props;
  const [componentSession] = useState(session);
  const [initSession, setInitSession] = useState(true);

  const [state, dispatch] = useReducer(reducer, initialState);
  const {session: stateSession} = state;

  useEffect(() => {
    dispatch({ type: 'SET_SESSION', payload: session });
    setInitSession(false);
  }, []);

  const set = useCallback( (data) => {
    return axios({
      method: 'post',
      url: '/api/session',
      data
    }).then((res) => {
      dispatch({
        type: 'SET_SESSION',
        payload: {
          ...stateSession,
          ...res.data.data
        }
      });
    });
  }, []);

  return (
    <Component {...props} session={{
      ...(initSession ? componentSession : stateSession),
      set
    }}/>
  );
};

export default withSessionDecorator;

export const getServerSideSessionProps = (req, params) => {
  const { session } = req;
  const paramsFromSession = {};
  params.forEach(param => {
    paramsFromSession[param] = session[param] || null;
  });
  return {session: paramsFromSession};
};
