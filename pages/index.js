import React from 'react';
import withSessionDecorator, { getServerSideSessionProps } from '../decorators/withSessionDecorator';
import { withSession } from 'next-session';

const Home = ({ session }) => {
  const loginSomeOne = (name) => () => {
    session.set({
      user_name: name
    })
  };

  return (
    <div className="container">
      <main>
        <h1 className="title">
          <span>{ session.user_name || "Nobody"}</span> is there!
        </h1>
        <button onClick={loginSomeOne("Glenn Matthews")}>Login Glenn Matthews</button>
        <button onClick={loginSomeOne("John Dorian")}>Login John Dorian</button>
      </main>

      <style>{`
      .container {
        min-height: 100vh;
        padding: 0 0.5rem;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }
      
      button {
        line-height: 32px;
        font-size: 16px;
        margin-top: 16px;
      }

      main {
        padding: 5rem 0;
        flex: 1;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }

      a {
        color: inherit;
        text-decoration: none;
      }

      .title span {
        color: #0070f3;
        text-decoration: none;
      }

      .title span:hover,
      .title span:focus,
      .title span:active {
        text-decoration: underline;
      }

      .title {
        margin: 0;
        line-height: 1.15;
        font-size: 4rem;
      }

      .title,
      .description {
        text-align: center;
      }

      .description {
        line-height: 1.5;
        font-size: 1.5rem;
      }
    `}</style>

      <style>{`
      html,
      body {
        padding: 0;
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
          Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
      }

      * {
        box-sizing: border-box;
      }
    `}</style>
    </div>
  );
};

export default withSessionDecorator(withSession(Home), ["user_name"]);

export async function getServerSideProps({ req }) {
  return {
    props: {
      ...getServerSideSessionProps(req, ["user_name"])
    }
  }
}
