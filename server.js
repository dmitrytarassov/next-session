const express = require('express');
const {session} = require('next-session');
const next = require('next');
const bodyParser = require('body-parser');
const axios = require('axios');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();

  // parse various different custom JSON types as JSON
  server.use(bodyParser.json());

  server.use(bodyParser.urlencoded({
    extended: true
  }));

  // add session middleware
  server.use(session());

  // add session route
  server.post("/api/session", (req, res) => {
    Object.keys(req.body)
      .forEach(param => {
        req.session[param] = req.body[param];
      });

    res.json({
      data: req.body
    });
  });

  server.all('*', (req, res) => {
    return handle(req, res)
  });

  server.listen(port, err => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });

});